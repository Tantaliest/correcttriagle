﻿using NUnit.Framework;
using static CorrectTriagle.Program;

namespace CorrectTriagleTests {
    [TestFixture]
    public class TestClass {
        [Test]

        public void NegativeNumber( ) {
            Assert.AreEqual(isTriagle(4, -2, 5), false);
        }

        public void TwoInfinity( ) {
            Assert.AreEqual(isTriagle(float.PositiveInfinity, 4, float.PositiveInfinity), false);
        }

        public void OneInfinity( ) {
            Assert.AreEqual(isTriagle(4, float.PositiveInfinity, 5), false);
        }

        public void NotNAN( ) {
            Assert.AreEqual(isTriagle(float.NaN, 4, 5), false);
        }

        public void Line( ) {
            Assert.AreEqual(isTriagle(4, 3, 7), false);
        }

        public void FewerBorders( ) {
            Assert.AreEqual(isTriagle(4, (float)2.99999, 7), false);
        }

        public void MuchFewerBorders( ) {
            Assert.AreEqual(isTriagle(1, 17, 23), false);
        }

        public void MoreBorders( ) {
            Assert.AreEqual(isTriagle(4, (float)3.00009, 7), true);
        }

        public void MuchMoreBorders( ) {
            Assert.AreEqual(isTriagle(11, 17, 23), true);
        }


        public void EquilateralTriangle( ) {
            Assert.AreEqual(isTriagle(14, 7, 14), true);
        }

    }
}
